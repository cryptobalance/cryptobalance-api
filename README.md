# Crypto Balance API

RESTful Express API for [Crypto Balancel](https://bitbucket.org/cryptobalance/cryptobalance) on top of MongoDB.

## Coins

**Note:** See `models/coin.js` for the Coin schema attributes.

| HTTP Verb | Path | Description |
|-----------|------|--------------|
| `GET` | `/coins` | Retrieve all coins |
| `POST` | `/coins/:id` | Create a coin* |
| `DELETE` | `/coins/:id` | Delete a single coin by it's `id` |

## Running Locally

Make sure you have:
 * [MongoDB](https://docs.mongodb.com/), [Yarn](https://yarnpkg.com/en/) and [NodeJS](https://nodejs.org/en/) installed.
 * [cryptobalance](https://bitbucket.org/cryptobalance/cryptobalance) installed

```bash
sudo service mongod start
git clone git@bitbucket.org:cryptobalance/cryptobalance-api.git
cd cryptobalance-api
yarn install
yarn start
```
